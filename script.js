const images = document.querySelectorAll('.image-to-show')
let curendSlide = 0
let intervalId = setInterval(() => {
  document.querySelector('.image-to-show.active').classList.remove('active')
  if (curendSlide === images.length - 1) {
    curendSlide = 0
  } else {
    curendSlide++
  }
  images[curendSlide].classList.add('active')
}, 500)
document.querySelector('.stop').addEventListener('click', () => {
  clearInterval(intervalId)
})
document.querySelector('.play').addEventListener('click', () => {
  intervalId = setInterval(() => {
    document.querySelector('.image-to-show.active').classList.remove('active')
    if (curendSlide === images.length - 1) {
      curendSlide = 0
    } else {
      curendSlide++
    }
    images[curendSlide].classList.add('active')
  }, 500)
})
